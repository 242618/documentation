# Quickstart

Quickstart is intended for users that want to spin up a virtual machine instance in the cloud as quickly as possible.

To create a virtual machine instance, you need to
* sign in via the MetaCentrum cloud web interface,
* create an *ssh* key pair or use an existing key pair,
* set up or update a security group,
* start a virtual machine instance,
* log in to the running instance using *ssh* (secure shell).

## Sign In
The web interface is available at [https://cloud.muni.cz](https://cloud.muni.cz).

To sign in,
* select `ENIFRA CESNET`,
* click on `Sign In`,
* select your institution from the drop-down list (use the search box at the top as a filter),
* provide your institution-specific sign-in credentials,
* wait to be redirected back to our web interface.

## Create Key Pair
All virtual machine instances running in the cloud have to be accessed remotely. The most common way of accessing a virtual machine instance remotely is the secure shell - *ssh*. Using *ssh* requires a pair of cryptographic keys.

1. To create a new *ssh* key pair, go to **Project &gt; Compute &gt; Key Pairs** and click the button "Create Key Pair"
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/CreateKeyPair1.png)
{% endreveal %}
2. Name your new key and click on "Create Key Pair" again. Avoid using special characters, if possible.
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/CreateKeyPair2.png)
{% endreveal %}
3. You can see your newly created key pair and its fingerprint. We recommend you store the key in a safe location and back it up in case you accidentally delete it.
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/CreateKeyPair3.png)
{% endreveal %}

## Update Security Group
In OpenStack, all incoming traffic from external networks to virtual machine instances is blocked by default. You need to explicitly allow access to individual virtual machine instances and services via a security group.

You need to add two new rules to be able to connect to your new instance (or any instance using the given security group). This is similar to setting up firewall rules on your router or server. If set up correctly, you will be able to access your virtual machine via *ssh* from your local terminal.

1. Go to **Project &gt;  Networks &gt; Security Groups**. Here you can see the default security group. You can either edit the default group by clicking on "Manage Rules" or create a new one. The simplest way is to edit the default group.
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/SecurityGroups1.png)
{% endreveal %}
2. Click on "Add rule", choose "SSH" and leave the remaining fields unchanged.
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/SecurityGroups2.png)
{% endreveal %}
3. Click on "Add rule", choose "ALL ICMP" and leave the remaining fields unchanged.
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/SecurityGroups3.png)
{% endreveal %}

## Start Instance
1. Navigate to **Quickstart** and click on "Launch Instance".
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/qs1.png)
{% endreveal %}
2. Choose a name for your virtual machine.
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/qs3-1.png)
{% endreveal %}
3. Choose an Operating System to run on your instance, also known as an **Image**.
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/qs3-2.png)
{% endreveal %}
4. Choose a suitable size of the new instance, also known as a **Flavor**.
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/qs3-3.png)
{% endreveal %}
5. Select the previously created *ssh* key pair by name.
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/qs3-4.png)
{% endreveal %}
6. Click on "Launch instance" and wait until **Quickstart** provides you with information about the running virtual machine instance.
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/qs4.png)
{% endreveal %}
7. *[Optionally]* Add a persistent volume to the running virtual machine instance.
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/qs5.png)
{% endreveal %}
8. Close the dialog with "Finish" to return to the list of active instances.
{% reveal text="Show Screenshot" %}
  ![](/0-quickstart/qs6.png)
{% endreveal %}

## Log In
Follow instructions provided by **Quickstart** and log in remotely:
> ssh -i ${PATH_TO_SSH_KEY_FILE} ${USER}@${IP_ADDRESS}

# Frequently Asked Questions

## I want to use OpenStack. How do I register?
Follow instructions for registering in [MetaCentrum](https://metavo.metacentrum.cz/en/application/index.html).

## I have an issue with OpenStack. Where do I report it?
First, try to search the documentation for an answer to your problem. If all else fails, open a ticket with [helpdesk@ics.muni.cz](mailto:helpdesk@ics.muni.cz). When contacting Helpdesk, always include your *username* (upper right corner of the web interface) and *domain* with active *project* (upper left corner of the web interface) as well as a description of your problem and/or an error messsage if available.

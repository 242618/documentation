# MetaCentrum Cloud Documentation
The MetaCentrum Cloud provides an environment for running your own virtual servers or whole virtual environments as a service. This infrastructure is available to anyone wishing to run *arbitrary* services, containerized application environments, unusual operating systems or complex computing infrastructures.

This service is owned and operated as a joint venture of CESNET, the national e-infrastructure for science, development and education in Czech Republic and Institute of Computer Science of Masaryk University.

![](/logos-cesnet.png)
![](/logos-muni.png)

## How-to
This guide aims to provide a basic walk-through for setting up your virtual environment. Several topics are covered. If you are a complete beginner we recommend starting in the [Quickstart](0-quickstart/README.md) section which will walk you through all the necessary activities step by step. Use the left sidebar for navigation throughout the documentation. You can also download individual pages to PDF for printing or later use.

Bear in mind that this is not the complete documentation to OpenStack but rather a quick guide that is supposed to help you with elementary use of our infrastructure. If you need more information, please turn to the official [OpenStack Documentation](https://docs.openstack.org/rocky/user/).

## Terms and Conditions
The service is accessible to all users of the CESNET e-infrastructure for science, development and education. The service includes creation of virtual servers on the OpenStack platform. The service does NOT include installation and management of an operating system or server applications. You may contact the service desk which may help you find a qualified administrator for your endeavor.

The following already established terms and conditions apply:
* [Terms and Conditions for Access to the CESNET e-infrastructure](https://www.cesnet.cz/conditions/?lang=en)
* [MetaCentrum End User Statement and Usage Rules](https://www.metacentrum.cz/en/about/rules/index.html)
* [Appreciation Formula / Acknowlegement in Publications](https://wiki.metacentrum.cz/wiki/Usage_rules/Acknowledgement)
* [Account Validity](https://wiki.metacentrum.cz/wiki/Usage_rules/Account)
* [Annual Report](https://wiki.metacentrum.cz/wiki/MetaCentrum_Annual_Report_%E2%88%92_Author_Instructions)

At the end of each year, the system will ask you to extend your account. You will be asked to fill out your usage description, description of achieved results and provide a list of publications for the last year.

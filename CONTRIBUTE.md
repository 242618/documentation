# How To Contribute
Working with our documentation requires the following tools:
* *git* for version control
* *nodejs* and *gitbook* for content management

This documentation is written in the *Markdown* markup language.

```bash
# Debian
apt-get install nodejs git
```
```bash
# CentOS
yum install nodejs git
```
```bash
# Fedora
dnf install nodejs git
```
Or see [NodeJS Documentation](https://nodejs.org/en/download/package-manager/) for distro-specific instructions.

## Work-flow Overview
1. Fork & clone repository
2. Create a branch
3. Commit your changes
4. Push to the branch
5. Create a Merge Request with the content of your branch

## Fork Repository
See [GitLab @ ICS MU](https://gitlab.ics.muni.cz/cloud/documentation/forks/new) for details. This will create your own clone of our repository where you will be able to make changes. Once you are happy with your changes, use GitLab to submit them to our original repository.

## Clone Repository
```bash
# after creating your own copy of the repository on GitLab
git clone git@gitlab.ics.muni.cz:${GITLAB_USER}/documentation.git
```

## Create New Branch
```bash
# in `documentation`
git checkout -b my_change
```

## Install GitBook
```bash
npm install gitbook-cli -g

# in `documentation`
gitbook install
```
This step MAY require `sudo` depending on your system and NodeJS installation method.

## Edit GitBook
```bash
# in `documentation`
gitbook serve
```
> Edits will be show live in your browser window, no need to refresh.

## Commit and Push Changes
```bash
git commit -am "My updates"
git push origin my_change
```

## Submit Changes
Create a *Merge Request* via [GitLab @ ICS MU](https://gitlab.ics.muni.cz/cloud/documentation/merge_requests/new).
